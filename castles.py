import typing
from dataclasses import dataclass
from typing import Callable as Fn


@dataclass
class RoomInCastle:
    x: int
    y: int
    name: str
    decoration: str
    room_type: str
    score: Fn  # Fn[[Room, Castle], int]

    def __hash__(self) -> int: return hash(self.name)


@dataclass
class Room:
    name: str
    decoration: str
    room_type: str
    score: Fn


Castle = list[RoomInCastle]


def score(castle: Castle):
    for room in castle:
        print(room.name, room.score(room, castle))
    return sum(room.score(room, castle) for room in castle)


def fountain(*args) -> int: return 5


def grand_foyer(room: RoomInCastle, castle: Castle) -> int:
    return sum(1 for other in castle if (abs(room.y - other.y) <= 1 and abs(room.x - other.x) <= 1)) - 1


def tower(room: RoomInCastle, castle: Castle) -> int:
    return sum(1 for other in castle if room.x == other.x) - 1


def sleeping_room(_, castle: Castle) -> int:
    room_types: set[str] = set(room.room_type for room in castle)
    if 'Kitchen' in room_types and \
            'Living' in room_types and \
            'Utility' in room_types and \
            'Outdoors' in room_types and \
            'Corridor' in room_types and \
            'Downstairs' in room_types:
        return 4
    else:
        return 1


def kitchen_h(room: RoomInCastle, castle: Castle, room_type: str, multiplier: int) -> int:
    return sum(multiplier for other in castle if
               abs(room.x - other.x) == 1 and room.y == other.y and other.room_type == room_type)


def kitchen_h_(room_type: str, multiplier: int):
    return lambda room, castle: kitchen_h(room, castle, room_type, multiplier)


def kitchen_v(room: RoomInCastle, castle: Castle, room_type: str, multiplier: int) -> int:
    return sum(multiplier for other in castle if
               abs(room.y - other.y) == 1 and room.x == other.x and other.room_type == room_type)


def kitchen_v_(room_type: str, multiplier: int):
    return lambda room, castle: kitchen_v(room, castle, room_type, multiplier)


def kitchen_down(room: RoomInCastle, castle: Castle, room_type: str, multiplier: int) -> int:
    return sum(multiplier for other in castle if room.y - other.y in (1, 2) and other.room_type == room_type)


def kitchen_down_(room_type: str, multiplier: int):
    return lambda room, castle: kitchen_down(room, castle, room_type, multiplier)


def living(room: RoomInCastle, castle: Castle, room_type: str, multiplier: int = 1) -> int:
    return sum(multiplier for other in castle if (abs(room.y - other.y) <= 1 and abs(room.x - other.x) <= 1)
               and not (other.x == room.x and other.y == room.y)
               and other.room_type == room_type)


def living_(room_type, multiplier: int = 1):
    return lambda room, castle: living(room, castle, room_type, multiplier)


def utility(room: RoomInCastle, castle: Castle, room_type: str, multiplier: int = 1) -> int:
    queue: list[RoomInCastle] = [room]
    counted: set[RoomInCastle] = set()
    while len(queue) > 0:
        curr: RoomInCastle = queue.pop()
        for other in castle:
            if abs(other.x - curr.x) + abs(other.y - curr.y) == 1 and \
                    other.room_type == room_type and other != room and other not in counted:
                counted.add(other)
                queue.append(other)
    return multiplier * len(counted)


def utility_(room_type: str, multiplier: int = 1):
    return lambda room, castle: utility(room, castle, room_type, multiplier)


def outdoors(room: RoomInCastle, castle: Castle, room_type: str, multiplier: int = 1) -> int:
    return multiplier * sum(1 for other in castle if other.room_type == room_type)


def outdoors_(room_type: str, multiplier: int = 1):
    return lambda room, castle: outdoors(room, castle, room_type, multiplier)


def corridor(room: RoomInCastle, castle: Castle, decor_type: str, multiplier: int = 1) -> int:
    return sum(multiplier for other in castle if
               (abs(room.y - other.y) <= 1 and abs(room.x - other.x) <= 1)
               and other.decoration == decor_type) - 1


def corridor_(decor_type: str, multiplier: int = 1):
    return lambda room, castle: corridor(room, castle, decor_type, multiplier)


def downstairs(room: RoomInCastle, castle: Castle, room_type: str, multiplier: int = 1) -> int:
    return sum(multiplier for other in castle if room.x == other.x and other.room_type == room_type and
               room is not other)


def downstairs_(room_type: str, multiplier: int = 1):
    return lambda room, castle: downstairs(room, castle, room_type, multiplier)


all_rooms = [
    Room('Spy Room', 'Torch', 'Downstairs', downstairs_('Downstairs')),
    Room('Between two Rooms', 'Torch', 'Corridor', corridor_('Torch')),
    Room('Key Room', 'Round', 'Utility', utility_('Corridor')),
    Room('Echo Chamber', 'Torch', 'Corridor', corridor_('Torch')),
    Room('Hall of Windows', 'Square', 'Corridor', corridor_('Square')),
    Room('Hall of Doors', 'Round', 'Corridor', corridor_('Round')),
    Room('Reception Room', 'Torch', 'Living', living_('Corridor')),
    Room('Hall of Portraits', 'Square', 'Corridor', corridor_('Square')),
    Room('Study', 'Round', 'Living', living_('Special')),
    Room('Dressing Room', 'Torch', 'Sleeping', sleeping_room),
    Room('Scullery', 'Torch', 'Food', kitchen_v_('Utility', 2)),
    Room('Prince\'s Chambers', 'Round', 'Sleeping', sleeping_room),
    Room('Kitchen', 'Torch', 'Food', kitchen_v_('Food', 2)),
    Room('Shield Room', 'Torch', 'Utility', utility_('Downstairs')),  # Ghosting
    Room('Wine Room', 'Round', 'Food', kitchen_v_('Sleeping', 2)),
    Room('Meditation Room', 'Swords', 'Living', living_('Outdoors', 2)),
    Room('Mold Room', 'Swords', 'Downstairs', downstairs_('Food')),
    Room('Fireplace', 'Round', 'Living', living_('Food')),
    Room('Solar', 'Round', 'Sleeping', sleeping_room),  # Blurry
    Room('Panic Room', 'Swords', 'Utility', utility_('Special')),
    Room('Pit of Despair', 'Round', 'Downstairs', downstairs_('Downstairs')),
    Room('Broom Closet', 'Torch', 'Utility', utility_('Sleeping')),
    Room('Rug Room', 'Square', 'Living', living_('Living')),
    Room('Treasure Room', 'Swords', 'Downstairs', downstairs_('Sleeping')),
    Room('Tapestry Room', 'Swords', 'Living', living_('Sleeping')),
    Room('Game Storage', 'Swords', 'Utility', utility_('Living')),
    Room('Schoolhouse', 'Torch', 'Utility', utility_('Corridor')),
    Room('Pantry', 'Swords', 'Food', kitchen_h_('Corridor', 2)),
    Room('Spice Room', 'Round', 'Food', kitchen_down_('Downstairs', 2)),
    Room('Biergarten', '', 'Outdoors', outdoors_('Special')),
    Room('Pacing Hall', 'Round', 'Corridor', corridor_('Round')),
    Room('Morning Tea Room', 'Swords', 'Food', kitchen_v_('Living', 2)),
    Room('Vineyard', '', 'Outdoors', outdoors_('Living')),
    Room('Sauerkraut Room', 'Round', 'Food', kitchen_h_('Food', 2)),
    Room('Turtle Pond', '', 'Outdoors', outdoors_('Sleeping')),
    Room('Crypt', 'Square', 'Downstairs', downstairs_('Downstairs')),
    Room('Hidden Passage', 'Torch', 'Corridor', corridor_('Torch')),
    Room('Hidden Entrance', 'Swords', 'Downstairs', downstairs_('Corridor')),
    Room('Buttery', 'Torch', 'Food', kitchen_v_('Corridor', 2)),  # Blurry
    Room('Brewery', 'Round', 'Food', kitchen_v_('Living', 2)),
    Room('Walking Path', '', 'Outdoors', outdoors_('Corridor')),
    Room('Cutlery Room', 'Square', 'Food', kitchen_h_('Outdoors', 2)),  # Ghosting
    Room('Kittenry', 'Square', 'Utility', utility_('Corridor')),
    Room('Promenade', 'Swords', 'Corridor', corridor_('Swords')),
    Room('Hall of Paintings', 'Square', 'Corridor', corridor_('Square')),  # Ghosting
    Room('Hidden Lair', 'Torch', 'Downstairs', downstairs_('Utility')),
    Room('Snake Pit', 'Round', 'Downstairs', downstairs_('Living')),  # Ghosting
    Room('King\'s Chambers', 'Torch', 'Sleeping', sleeping_room),
    Room('Fungus Room', 'Round', 'Downstairs', downstairs_('Food')),
    Room('Stables', '', 'Outdoors', outdoors_('Downstairs')),
    Room('Servant\'s Quarters', 'Square', 'Sleeping', sleeping_room),
    Room('Dumbwaiter', 'Swords', 'Utility', utility_('Food')),
    Room('Puppy Room', 'Square', 'Sleeping', sleeping_room),
    Room('Crepery', 'Swords', 'Food', kitchen_v_('Sleeping', 2)),
    Room('Swimming Hole', '', 'Outdoors', outdoors_('Living')),  # I turned off VR probably here, or at Jewel Room
    Room('Jewel Room', 'Swords', 'Downstairs', downstairs_('Living')),
    Room('Bunk Room', 'Square', 'Sleeping', sleeping_room),
    Room('Tool Room', 'Round', 'Utility', utility_('Utility')),
    Room('Kennel', 'Square', 'Utility', utility_('Sleeping')),
    Room('Vestibule', 'Swords', 'Living', living_('Food')),
    Room('Map Room', 'Round', 'Living', living_('Sleeping')),
    Room('Blanket Room', 'Square', 'Sleeping', sleeping_room),
    Room('Glassmaking Room', 'Round', 'Utility', utility_('Utility')),
    Room('Waiting Room', 'Round', 'Living', living_('Corridor')),
    Room('Dead End', 'Swords', 'Corridor', corridor_('Swords')),
    Room('Hall of Ever-closing Walls', 'Swords', 'Corridor', corridor_('Swords')),
    Room('Venus Grotto', 'Torch', 'Downstairs', downstairs_('Special', 2)),
    Room('Cape Room', 'Square', 'Living', living_('Underground', 2)),
    Room('Hall of Sculptures', 'Round', 'Corridor', corridor_('Round')),
    Room('Hibernation Chamber', 'Square', 'Sleeping', sleeping_room),
    Room('Gunpowder Room', 'Square', 'Downstairs', downstairs_('Corridor')),
    Room('Purgatory', 'Torch', 'Corridor', corridor_('Torch')),
    Room('Grand Balcony', '', 'Outdoors', outdoors_('Sleeping')),
    Room('Princess\' Chambers', 'Round', 'Sleeping', sleeping_room),
    Room('Dungeon', 'Round', 'Downstairs', downstairs_('Corridor')),
    Room('Afternoon Tea Room', 'Torch', 'Food', kitchen_v_('Sleeping', 2)),
    Room('Sauna', 'Swords', 'Utility', utility_('Sleeping')),
    Room('Nursery', 'Torch', 'Sleeping', sleeping_room),
    Room('Lockdown Room', 'Swords', 'Corridor', corridor_('Swords')),
    Room('Midnight Room', 'Swords', 'Sleeping', sleeping_room),
    Room('Parlor', 'Torch', 'Living', living_('Food')),
    Room('Mud Room', 'Square', 'Utility', utility_('Outdoors')),
    Room('Aviary', '', 'Outdoors', outdoors_('Utility')),
    Room('Powder Room', 'Round', 'Utility', utility_('Food')),
    Room('Great Hall', 'Swords', 'Corridor', corridor_('Swords')),
    Room('Siege Food Storage', 'Swords', 'Food', kitchen_v_('Special', 2)),
    Room('Gallery', 'Swords', 'Living', living_('Utility')),
    Room('Scythe Room', 'Square', 'Utility', utility_('Downstairs')),
    Room('Fish Pond', '', 'Outdoors', outdoors_('Food')),
    Room('Chicken Coop', '', 'Outdoors', outdoors_('Utility')),
    Room('Ice House', 'Torch', 'Food', kitchen_h_('Living', 2)),
    Room('Bakery', 'Swords', 'Food', kitchen_h_('Utility', 2)),
    Room('Coat Room', 'Round', 'Utility', utility_('Outdoors')),
    Room('Escape Room', 'Square', 'Corridor', corridor_('Square')),
    Room('Armory', 'Square', 'Downstairs', downstairs_('Special', 2)),
    Room('Pumpkin Garden', '', 'Outdoors', outdoors_('Food')),
    Room('Winter Garden', '', 'Outdoors', outdoors_('Utility')),
    Room('Children\'s Room', 'Swords', 'Sleeping', sleeping_room),
    Room('Sitting Room', 'Round', 'Living', living_('Outdoors', 2)),
    Room('Nap Room', 'Torch', 'Sleeping', sleeping_room),
    Room('Guardhouse', '', 'Outdoors', outdoors_('Downstairs')),
    Room('Salon', 'Square', 'Living', living_('Sleeping')),
    Room('Dining Room', 'Square', 'Food', kitchen_v_('Food', 2)),
    Room('Subterranean Tunnel', 'Square', 'Downstairs', downstairs_('Utility')),
    Room('Hall of Creaking Floors', 'Square', 'Corridor', corridor_('Square')),
    Room('Breakfast Nook', 'Square', 'Food', kitchen_v_('Corridor', 2)),
    Room('Pillow Room', 'Torch', 'Sleeping', sleeping_room),
    Room('Quiet Room', 'Torch', 'Downstairs', downstairs_('Sleeping')),
    Room('Terrace', '', 'Outdoors', outdoors_('Sleeping')),
    Room('Wine Cellar', 'Torch', 'Downstairs', downstairs_('Food')),
    Room('Dreaming Room', 'Square', 'Sleeping', sleeping_room),
    Room('Reading Room', 'Torch', 'Living', living_('Outdoors', 2)),
    Room('Library', 'Square', 'Living', living_('Special')),
    Room('French Gazebo', '', 'Outdoors', outdoors_('Special')),
    Room('Meat Locker', 'Swords', 'Food', kitchen_h_('Outdoors', 2)),
    Room('Washroom', 'Round', 'Utility', utility_('Living')),
    Room('Harp Room', 'Round', 'Living', living_('Utility')),
    Room('Knight Room', 'Round', 'Downstairs', downstairs_('Special', 2)),
    Room('Vegetable Garden', '', 'Outdoors', outdoors_('Food')),
    Room('Flower Garden', '', 'Outdoors', outdoors_('Special')),
    Room('The Hole', 'Square', 'Downstairs', downstairs_('Sleeping')),
    Room('Butterfly Garden', '', 'Outdoors', outdoors_('Corridor')),
    Room('Crown Storage', 'Square', 'Utility', utility_('Special')),
    Room('Hall of Ghosts', 'Round', 'Corridor', corridor_('Round')),
    Room('In-law suite', 'Swords', 'Sleeping', sleeping_room),
    Room('Hat Room', 'Square', 'Living', living_('Corridor')),
    Room('Hall of Mirrors', 'Round', 'Corridor', corridor_('Round')),
    Room('Observatory', 'Torch', 'Living', living_('Living')),
    Room('Chocolate Room', 'Square', 'Food', kitchen_down_('Downstairs', 2)),
    Room('Sty', '', 'Outdoors', outdoors_('Downstairs')),
    Room('Archery Range', '', 'Outdoors', outdoors_('Corridor')),
    Room('Laundry Room', 'Torch', 'Utility', utility_('Food')),
    Room('Torch Storage', 'Torch', 'Utility', utility_('Living')),
    Room('Rabbit Room', 'Swords', 'Sleeping', sleeping_room),
    Room('Hall of Puzzled Floors', 'Torch', 'Corridor', corridor_('Torch')),
    Room('Tasso Room', 'Round', 'Sleeping', sleeping_room),
    Room('China Room', 'Round', 'Food', kitchen_h_('Special', 2)),
    Room('Repair Shop', 'Swords', 'Utility', utility_('Utility')),
    Room('Laboratory', 'Swords', 'Downstairs', downstairs_('Utility')),
    Room('Drawing Room', 'Square', 'Living', living_('Utility')),
    Room('Padded Room', 'Torch', 'Downstairs', downstairs_('Living')),
    Room('Taxidermy Showroom', 'Torch', 'Living', living_('Downstairs', 2)),
    Room('Hall of Knights', 'Square', 'Corridor', corridor_('Square')),
    Room('Firewood Storage', '', 'Outdoors', outdoors_('Living')),
    Room('Queen\'s Chambers', 'Round', 'Sleeping', sleeping_room),
    Room('Guest Bedroom', 'Swords', 'Sleeping', sleeping_room),
    Room('Brandy Room', 'Square', 'Food', kitchen_v_('Special', 2)),
    Room('Grand Foyer', '', 'Special', grand_foyer),
    Room('Fountain', '', 'Special', fountain),
    Room('Tower', '', 'Special', tower),
]

all_thronerooms = [
    Room('Throneroom CF left', 'Swords', 'Special', lambda room, castle:
         sum(2 for other in castle if (other.x, other.y) == (0, 1) and other.room_type in ('Food', 'Corridor'))),
    Room('Throneroom CF right', 'Swords', 'Special', lambda room, castle:
         sum(2 for other in castle if (other.x, other.y) == (1, 1) and other.room_type in ('Food', 'Corridor'))),

    Room('Throneroom LC left', 'Torch', 'Special', lambda room, castle:
         sum(2 for other in castle if (other.x, other.y) == (-1, 0) and other.room_type in ('Living', 'Corridor'))),
    Room('Throneroom LC right', 'Torch', 'Special', lambda room, castle:
         sum(2 for other in castle if (other.x, other.y) == (2, 0) and other.room_type in ('Living', 'Corridor'))),

    Room('Throneroom UF left', 'Square', 'Special', lambda room, castle:
         sum(2 for other in castle if (other.x, other.y) == (-1, 0) and other.room_type in ('Utility', 'Food'))),
    Room('Throneroom UF right', 'Square', 'Special', lambda room, castle:
         sum(2 for other in castle if (other.x, other.y) == (2, 0) and other.room_type in ('Utility', 'Food'))),

    Room('Throneroom CD left', 'Round', 'Special', lambda room, castle:
         sum(2 for other in castle if (other.x, other.y) == (0, -1) and other.room_type in ('Corridor', 'Downstairs'))),
    Room('Throneroom CD right', 'Round', 'Special', lambda room, castle:
         sum(2 for other in castle if (other.x, other.y) == (1, -1) and other.room_type in ('Corridor', 'Downstairs'))),

    Room('Throneroom FS left', 'Round', 'Special', lambda room, castle:
         sum(2 for other in castle if (other.x, other.y) == (-1, 0) and other.room_type in ('Food', 'Sleeping'))),
    Room('Throneroom FS right', 'Round', 'Special', lambda room, castle:
         sum(2 for other in castle if (other.x, other.y) == (0, 1) and other.room_type in ('Food', 'Sleeping'))),

    Room('Throneroom LS left', 'Swords', 'Special', lambda room, castle:
         sum(2 for other in castle if (other.x, other.y) == (0, 1) and other.room_type in ('Living', 'Sleeping'))),
    Room('Throneroom LS right', 'Swords', 'Special', lambda room, castle:
         sum(2 for other in castle if (other.x, other.y) == (1, 1) and other.room_type in ('Living', 'Sleeping'))),

    Room('Throneroom US left', 'Torch', 'Special', lambda room, castle:
         sum(2 for other in castle if (other.x, other.y) == (1, 1) and other.room_type in ('Utility', 'Sleeping'))),
    Room('Throneroom US right', 'Torch', 'Special', lambda room, castle:
         sum(2 for other in castle if (other.x, other.y) == (2, 0) and other.room_type in ('Utility', 'Sleeping'))),
]


def c(room: Room | str | int, x: int, y: int) -> RoomInCastle:
    if type(room) is str:
        assert len((room := [r for r in all_rooms if r.name.lower() == room.lower()])) == 1
        room = room[0]
    if type(room) is int:
        room = all_rooms[room]
    return RoomInCastle(x, y, room.name, room.decoration, room.room_type, room.score)


assert (s := score(Castle([]))) == 0, str(s)
assert (s := score(Castle([c('In-law suite', 0, 0)]))) == 1, str(s)
assert (s := score(Castle([c('In-law suite', 0, 0), c('Turtle Pond', 0, 1)]))) == 2, str(s)
assert (s := score(Castle([c('In-law suite', 0, 0), c('Turtle Pond', 1, 0), c('Meditation room', 2, 0)]))) == 4, str(s)
assert (s := score(Castle([c('In-law suite', 0, 0), c('Turtle Pond', 1, 0), c('Meditation room', 1, -1)]))) == 4, str(s)
assert (s := score(Castle([c('In-law suite', 0, 0), c('Turtle Pond', 1, 0), c('Meditation room', -1, 0)]))) == 2, str(s)


def royal_attendant(decoration: str, multiplier: int = 1) -> typing.Callable[[Castle], int]:
    return lambda castle: sum(multiplier for room in castle if room.decoration == decoration)


def score_with_bonuses(castle: Castle, bonuses: list[typing.Callable[[Castle], int]]):
    return score(castle) + sum(bonus(castle) for bonus in bonuses)
