import os
import sys
import getopt
from pathlib import Path
import cv2 as cv
import os

from multiprocessing.pool import ThreadPool
import numpy

from find_obj import init_feature, filter_matches

import asift
import castles
from config import *

opts, _ = getopt.getopt(sys.argv[1:], '', ['feature='])
opts = dict(opts)

# Analyse image
assert isinstance(img_folder, Path)
img_filenames: list[str] = os.listdir(img_folder)[(offset := 0): offset + 9999]
imgs = [cv.imread(str(img_folder / filename), cv.IMREAD_GRAYSCALE) for filename in img_filenames]

if any(img is None for img in imgs):
    print('Failed to load an image!')
    sys.exit(1)

detector, matcher = init_feature(opts.get('--feature', 'brisk-flann'))
pool = ThreadPool(processes=cv.getNumberOfCPUs())
anals: list[tuple[list, numpy.ndarray]] = [asift.affine_detect(detector, img, pool=pool) for img in imgs]
# Save image data: not yet

# Analyse castle
assert isinstance(castle_filename, str)
castle: numpy.ndarray = cv.imread(castle_filename, cv.IMREAD_GRAYSCALE)
if castle is None:
    print('Failed to load castle!')
    sys.exit(1)
kp_castle, desc_castle = asift.affine_detect(detector, castle, pool=pool)

# Match tiles (regular rooms)
matched = []

for (kp_img, desc_img), i in zip(anals, range(99999)):
    found: bool = True
    while found:
        found = False
        raw_matches = matcher.knnMatch(desc_img, trainDescriptors=desc_castle, k=2)
        p1, p2, kp_pairs = filter_matches(kp_img, kp_castle, raw_matches)
        if len(p1) >= 4:
            H, status = cv.findHomography(p1, p2, cv.RANSAC, 5.0)
            kp_pairs_filtered = [kpp for kpp, flag in zip(kp_pairs, status) if flag]
            if len(kp_pairs_filtered) >= 10:
                matched.append((i, H, kp_pairs_filtered, imgs[i].shape[1]))
                print(i, len(kp_pairs_filtered))
                ul = numpy.dot(H, numpy.array([0, 0, 1]))
                ll = numpy.dot(H, numpy.array([0, dim := matched[-1][3], 1]))
                ur = numpy.dot(H, numpy.array([dim, 0, 1]))
                lr = numpy.dot(H, numpy.array([dim, dim, 1]))
                process = lambda *items: tuple((a / a[2])[:2] for a in items)
                ul, ll, ur, lr = process(ul, ll, ur, lr)
                xs = sorted([ul[0], ll[0], ur[0], lr[0]])
                ys = sorted([ul[1], ll[1], ur[1], lr[1]])
                if len(castles.all_rooms) - 3 <= i + offset < len(castles.all_rooms):
                    for row in range(len(castle)):
                        for col in range(len(castle[row])):
                            if sorted([*ys, row])[2] == row and \
                                    sorted([*xs, col])[2] == col:
                                castle[row][col] = 0
                    kp_castle, desc_castle = asift.affine_detect(detector, castle, pool=pool)
                    found = True
        if len(matched) == 0 or matched[-1][0] != i:
            print(i, 'Nope!')

# Find throneroom
assert isinstance(throne_folder, Path)
throne_filenames: list[str] = os.listdir(throne_folder)
throne_imgs = [cv.imread(str(throne_folder / filename), cv.IMREAD_GRAYSCALE) for filename in throne_filenames]

if any(img is None for img in throne_imgs):
    print('Failed to load a throne room image!')
    sys.exit(1)
if len(throne_imgs) == 0: print('Failed to load load any images'), sys.exit(1)

throne_anals: list[tuple[list, numpy.ndarray]] = [asift.affine_detect(detector, img, pool=pool) for img in throne_imgs]

# Match throne rooms
matched_thronerooms = []
for (kp_img, desc_img), i in zip(throne_anals, range(9999999)):
    raw_matches_throne = matcher.knnMatch(desc_img, trainDescriptors=desc_castle, k=2)
    p1, p2, kp_pairs = filter_matches(kp_img, kp_castle, raw_matches_throne)
    if len(p1) >= 4:
        H, status = cv.findHomography(p1, p2, cv.RANSAC, 5.0)
        # do not draw outliers (there will be a lot of them)
        kp_pairs_filtered = [kpp for kpp, flag in zip(kp_pairs, status) if flag]
        if len(kp_pairs_filtered) >= 10:
            matched_thronerooms.append((i, H, kp_pairs_filtered, *imgs[i].shape[:2]))
            print(i, len(kp_pairs_filtered))
    if len(matched_thronerooms) == 0 or matched_thronerooms[-1][0] != i:
        print(i, 'Royal Throne Nope!')

print('Thronerooms matched:', len(matched_thronerooms))
print('Best match:', throneroom := [i for i, H, kpp, sh1, sh2 in matched_thronerooms
                                    if len(kpp) == max(len(kpp) for _, _, kpp, _, _ in matched_thronerooms)][0])

# Organize data into array / grid
tiles: list[dict[str, int | numpy.ndarray]] = []
ups, downs, lefts, rights = [], [], [], []
heights, widths = [], []

process = lambda *items: tuple((a / a[2])[:2] for a in items)

for i, H, kp_pairs_filtered, dim in matched:
    tile = castles.all_rooms[i + offset]
    x_avg: float = sum(kp.pt[0] for _, kp in kp_pairs_filtered) / len(kp_pairs_filtered)
    y_avg: float = sum(kp.pt[1] for _, kp in kp_pairs_filtered) / len(kp_pairs_filtered)

    ul = numpy.dot(H, numpy.array([0, 0, 1]))
    ll = numpy.dot(H, numpy.array([0, dim, 1]))
    ur = numpy.dot(H, numpy.array([dim, 0, 1]))
    lr = numpy.dot(H, numpy.array([dim, dim, 1]))
    ul, ll, ur, lr = process(ul, ll, ur, lr)

    # print(tile.name, 'X', x_avg, 'Y', y_avg, 'Dim', dim1)
    ups.append((i, up := ((ul + ur) / 2)))
    downs.append((i, down := ((ll + lr) / 2)))
    lefts.append((i, left := ((ul + ll) / 2)))
    rights.append((i, right := ((ur + lr) / 2)))
    tiles.append({'id': i, 'up': up, 'down': down, 'left': left, 'right': right})

    heights.append(numpy.linalg.norm(ul - ll))
    heights.append(numpy.linalg.norm(ur - lr))
    widths.append(numpy.linalg.norm(ul - ur))
    widths.append(numpy.linalg.norm(ll - lr))

min_height, min_width = min(heights), min(widths)
max_height, max_width = max(heights), max(widths)
min_dist = min(min_width, min_height)
print(min_dist)


def try_find_other(pos: numpy.array, direction: str) -> dict[str, int | numpy.ndarray] | None:
    if direction not in ('up', 'down', 'left', 'right'):
        raise ValueError()
    opposites = {'up': 'down', 'down': 'up', 'left': 'right', 'right': 'left'}

    if len(val := [tile for tile in tiles if numpy.linalg.norm(pos - tile[opposites[direction]]) < min_dist]) > 0:
        return val[0]
    return None


dirs = {'up': lambda x, y: (x, y + 1),
        'down': lambda x, y: (x, y - 1),
        'left': lambda x, y: (x - 1, y),
        'right': lambda x, y: (x + 1, y)}

# Process throne room, but differently:
for i, H, kp_pairs_filtered, dim1, dim2 in matched_thronerooms:
    x_avg: float = sum(kp.pt[0] for _, kp in kp_pairs_filtered) / len(kp_pairs_filtered)
    y_avg: float = sum(kp.pt[1] for _, kp in kp_pairs_filtered) / len(kp_pairs_filtered)

    ul = numpy.dot(H, numpy.array([0, 0, 1]))
    ll = numpy.dot(H, numpy.array([0, dim1, 1]))
    ur = numpy.dot(H, numpy.array([dim2, 0, 1]))
    lr = numpy.dot(H, numpy.array([dim2, dim1, 1]))
    ul, ll, ur, lr = process(ul, ll, ur, lr)

    # print(tile.name, 'X', x_avg, 'Y', y_avg, 'Dim', dim1)
    up = ((ul * 3 / 4) + (ur * 1 / 4))
    up2 = ((ul * 1 / 4) + (ur * 3 / 4))
    down = ((ll * 3 / 4) + (lr * 1 / 4))
    down2 = ((ll * 1 / 4) + (lr * 3 / 4))
    left = ((ul + ll) / 2)
    right = ((ur + lr) / 2)

    queue: list = []
    x, y = 0, 0
    castle: castles.Castle = [castles.c(castles.all_thronerooms[0 + (throneroom * 2)], 0, 0),
                              castles.c(castles.all_thronerooms[1 + (throneroom * 2)], 1, 0)]

    for midp, direction in ((up, 'up'), (down, 'down'), (left, 'left'), (right, 'right')):
        if (other := try_find_other(midp, direction)) is not None:
            queue.append((other, *dirs[direction](x, y)))
            tiles.remove(other)
            castle.append(castles.c(other['id'] + offset, *dirs[direction](x, y)))
    for midp, direction in ((up2, 'up'), (down2, 'down')):
        if (other := try_find_other(midp, direction)) is not None:
            queue.append((other, *dirs[direction](x + 1, y)))
            tiles.remove(other)
            castle.append(castles.c(other['id'] + offset, *dirs[direction](x + 1, y)))
    break  # Only process 1st found throne room, they are all similar anyway

while len(queue) > 0:
    room, x, y = queue.pop()
    for direction in dirs:
        if (other := try_find_other(room[direction], direction)) is not None:
            queue.append((other, *dirs[direction](x, y)))
            # tiles.remove(other) insead vvv
            tiles = [tile for tile in tiles if not (tile['id'] == other['id'] and
                     tile['up'][0] == other['up'][0] and
                     tile['down'][0] == other['down'][0] and
                     tile['left'][0] == other['left'][0] and
                     tile['right'][0] == other['right'][0])]
            castle.append(castles.c(other['id'] + offset, *dirs[direction](x, y)))

print('\n'.join(str(room) for room in castle))
print('Score', castles.score(castle))
