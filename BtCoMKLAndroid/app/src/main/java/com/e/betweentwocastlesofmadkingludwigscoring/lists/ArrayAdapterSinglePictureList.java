package com.e.betweentwocastlesofmadkingludwigscoring.lists;

import android.content.Context;
import android.content.res.TypedArray;
import android.view.View;
import android.widget.ImageButton;

import com.e.betweentwocastlesofmadkingludwigscoring.listItemClickHandler;
import com.e.betweentwocastlesofmadkingludwigscoring.R;

import java.util.ArrayList;

public class ArrayAdapterSinglePictureList extends ArrayAdapterGeneral<Integer>
{
    protected TypedArray picturesArray;


    public ArrayAdapterSinglePictureList(Context context, listItemClickHandler activity, int listCode, int picturesResource)
    {
        super(context, listCode, R.layout.list_item_single_picture, activity);
        this.picturesArray = context.getResources().obtainTypedArray(picturesResource);
    }

    @Override
    protected View arrangeDataInView(View currentListItem, int index)
    {
        ImageButton btnSingleImage = currentListItem.findViewById(R.id.img_list_single_picture);
        btnSingleImage.setBackgroundResource(picturesArray.getResourceId(data.get(index),0));
        btnSingleImage.setOnClickListener(new OnListItemClickListener(index, activity, listCode));
        return currentListItem;
    }

    protected void fillInDataWithInts()
    {
        data = new ArrayList<>();
        for (int i=0; i<picturesArray.length(); i++) //please tell me there is a more efficient solution to this!
        {
            data.add(i);
        }
    }

    public void recyclePictureArray()
    {
        picturesArray.recycle();
    }
}
