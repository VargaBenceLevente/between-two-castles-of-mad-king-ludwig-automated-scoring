package com.e.betweentwocastlesofmadkingludwigscoring.lists;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import android.widget.ScrollView;

public class ListGeneral<T> extends ScrollView implements ViewWithAdapter<T>
{
    protected ArrayAdapterGeneral<T> adapter;
    protected Context context;
    protected LinearLayout llyo;

    public ListGeneral(Context context)
    {
        super(context);
        init(context);
    }

    public ListGeneral(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        init(context);
    }

    public ListGeneral(Context context, AttributeSet attrs, int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    public ListGeneral(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes)
    {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context);
    }

    protected void init(Context context)
    {
        this.context = context;
        llyo = new LinearLayout(context);
        llyo.setOrientation(LinearLayout.VERTICAL);
        this.addView(llyo);
    }

    @Override
    public void setAdapter(ArrayAdapterGeneral<T> adapter)
    {
        this.adapter = adapter;
        adapter.setLlyo(llyo);
        adapter.populateList();
    }
}
