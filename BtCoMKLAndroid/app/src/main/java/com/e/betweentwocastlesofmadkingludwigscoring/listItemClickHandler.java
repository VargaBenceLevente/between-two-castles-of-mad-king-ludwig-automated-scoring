package com.e.betweentwocastlesofmadkingludwigscoring;

public interface listItemClickHandler
{
    public void handleListItemClick(int listIndex, int callingListCode);
}
