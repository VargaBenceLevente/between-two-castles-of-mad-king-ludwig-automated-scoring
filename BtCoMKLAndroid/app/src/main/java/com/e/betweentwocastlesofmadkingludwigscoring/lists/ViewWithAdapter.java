package com.e.betweentwocastlesofmadkingludwigscoring.lists;

public interface ViewWithAdapter<T>
{
    public void setAdapter(ArrayAdapterGeneral<T> adapter);
}
