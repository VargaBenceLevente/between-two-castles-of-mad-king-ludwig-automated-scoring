package com.e.betweentwocastlesofmadkingludwigscoring;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.FileProvider;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.TypedArray;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.e.betweentwocastlesofmadkingludwigscoring.lists.ArrayAdapterAvailableBonusCards;
import com.e.betweentwocastlesofmadkingludwigscoring.lists.ArrayAdapterGeneral;
import com.e.betweentwocastlesofmadkingludwigscoring.lists.ArrayAdapterSelectedBonusCards;
import com.e.betweentwocastlesofmadkingludwigscoring.lists.ArrayAdapterThroneRoom;
import com.e.betweentwocastlesofmadkingludwigscoring.lists.ListGeneral;
import com.e.betweentwocastlesofmadkingludwigscoring.lists.ListSelectedBonusCards;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CreateNewCastle extends AppCompatActivity implements listItemClickHandler
{
    //picture taking stuff
    private static final int PHOTO_REQUEST_CODE = 12;
    private static final int CAMERA_PERMISSION_CODE = 13;
    private boolean cameraPermission;

    //views
    private View backgroundShield;

    //photo
    private ImageButton btnTakePhoto;
    private TextView txtInstructionPhoto;
    private ScrollView scrViewCastleDetails;

    //castle name
    EditText edtCastleName;

    //architects
    private EditText edtArchitectLeft;
    private EditText edtArchitectRight;

    //throne room
    private Button btnSelectThroneRoom;
    private ListGeneral<Integer> listThroneRoom;
    private ArrayAdapterThroneRoom adapterThroneRoom;

    //attendants
    private boolean isFirstAttendantClicked = false;
    private ConstraintLayout cloAttendants;
    private Button btnAddFirstAttendant;
    private Button btnAddSecondAttendant;

    //bonus cards
    private ListGeneral<Integer> listAvailableBonusCards;
    private ListSelectedBonusCards listSelectedBonusCards;
    private ArrayAdapterAvailableBonusCards adapterAvailableBonusCards;
    private ArrayAdapterSelectedBonusCards adapterSelectedBonusCards;


    //other
    private Castle castle = new Castle();

    Button btnCheck;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_new_castle);

        //taking photo
        scrViewCastleDetails = findViewById(R.id.scroll_view_castle_details);
        btnTakePhoto = findViewById(R.id.btn_photo);
        btnTakePhoto.setOnClickListener(view ->
            {
                getCameraPermission();
                if(cameraPermission)
                {
                    try
                    {
                        takePhoto();
                    }
                    catch (IOException e)
                    {
                        Toast.makeText(CreateNewCastle.this, "Some error occurred while taking a photo", Toast.LENGTH_LONG).show();
                        e.printStackTrace();
                    }
                }
            }
        );

        //castle name
        Button btnRandomName = findViewById(R.id.btn_random_generate);
        edtCastleName = findViewById(R.id.edt_castle_name);
        btnRandomName.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                edtCastleName.setText((new CastleName()).GenerateCastleName());
            }
        });

        //architects
        edtArchitectLeft = findViewById(R.id.edt_architect_left);
        edtArchitectRight = findViewById(R.id.edt_architect_right);

        edtArchitectLeft.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2)
            {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2)
            {

            }

            @Override
            public void afterTextChanged(Editable editable)
            {
                String edtContent = edtArchitectLeft.getText().toString();
                if(!edtContent.equals(""))
                {
                    castle.setArchitectLeft(edtContent);
                }
            }
        });

        edtArchitectRight.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2)
            {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2)
            {

            }

            @Override
            public void afterTextChanged(Editable editable)
            {
                String edtContent = edtArchitectRight.getText().toString();
                if(!edtContent.equals(""))
                {
                    castle.setArchitectRight(edtContent);
                }
            }
        });


        //select throne room
        backgroundShield = findViewById(R.id.background_shield);
        listThroneRoom = findViewById(R.id.list_throne_room);
        adapterThroneRoom = new ArrayAdapterThroneRoom(CreateNewCastle.this, this);
        listThroneRoom.setAdapter(adapterThroneRoom);

        btnSelectThroneRoom = findViewById(R.id.btn_select_throne_room);
        btnSelectThroneRoom.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                backgroundShield.setVisibility(View.VISIBLE);
                listThroneRoom.setVisibility(View.VISIBLE);
            }
        });

        //add royal attendants
        cloAttendants = findViewById(R.id.clo_attendants);
        btnAddFirstAttendant = findViewById(R.id.btn_add_first_attendant);
        btnAddSecondAttendant = findViewById(R.id.btn_add_second_attendant);

        ImageButton btnAttendant0 = findViewById(R.id.btn_attendant0);
        ImageButton btnAttendant1 = findViewById(R.id.btn_attendant1);
        ImageButton btnAttendant2 = findViewById(R.id.btn_attendant2);
        ImageButton btnAttendant3 = findViewById(R.id.btn_attendant3);
        Button btnRemoveAttendant = findViewById(R.id.btn_remove_attendant);

        btnAddFirstAttendant.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                openAttendants();
                isFirstAttendantClicked = true;
            }
        });

        btnAddSecondAttendant.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                openAttendants();
                isFirstAttendantClicked = false;
            }
        });

        btnAttendant0.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                handleAttendantClick(0);
            }
        });

        btnAttendant1.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                handleAttendantClick(1);
            }
        });

        btnAttendant2.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                handleAttendantClick(2);
            }
        });

        btnAttendant3.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                handleAttendantClick(3);
            }
        });

        btnRemoveAttendant.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                //if the user is setting first attendant and it was already selected
                if(isFirstAttendantClicked && castle.getFirstAttendant() != -1)
                {
                    //if there is no second attendant
                    if(castle.getSecondAttendant()==-1)
                    {
                        resetAttendantButton(btnAddFirstAttendant);
                        castle.setFirstAttendant(-1);
                        btnAddSecondAttendant.setVisibility(View.INVISIBLE);
                    }
                    //if there is a second attendant, we switch them
                    else
                    {
                        castle.setFirstAttendant(castle.getSecondAttendant());
                        setAttendantButtonPicture(btnAddFirstAttendant, castle.getFirstAttendant());
                        resetAttendantButton(btnAddSecondAttendant);
                        castle.setSecondAttendant(-1);
                    }
                }
                //if the user is setting second attendant and it was already selected
                else if(!isFirstAttendantClicked && castle.getSecondAttendant() != -1)
                {
                    resetAttendantButton(btnAddSecondAttendant);
                    castle.setSecondAttendant(-1);
                }
                closeAttendants();
            }
        });

        //bonus cards
        listAvailableBonusCards = findViewById(R.id.list_available_bonus_cards);
        adapterAvailableBonusCards = new ArrayAdapterAvailableBonusCards(this, this);
        listAvailableBonusCards.setAdapter(adapterAvailableBonusCards);

        listSelectedBonusCards = findViewById(R.id.list_selected_bonuscards);
        adapterSelectedBonusCards = new ArrayAdapterSelectedBonusCards(this, this);
        listSelectedBonusCards.setAdapter(adapterSelectedBonusCards);

        btnCheck = findViewById(R.id.btn_evaluate);
        btnCheck.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                btnCheck.setText(castle.toString());
            }
        });
    }

    private void getCameraPermission()
    {
        if(checkSelfPermission(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED)
        {
            cameraPermission = true;
        }
        else
        {
            requestPermissions(new String[]{Manifest.permission.CAMERA}, CAMERA_PERMISSION_CODE);
        }
    }

    private void takePhoto() throws IOException
    {
        File photoDirectory = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        Uri photoFileUri = getPhotoFileUri(photoDirectory);
        castle.setPictureUri(photoFileUri);

        //Turns out, this MediaStore.ACTION_IMAGE_CAPTURE thingy actually also saves the picture for us, so don't look for the code, where the saving happens. Cool
        Intent takePhotoIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        takePhotoIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoFileUri);
        startActivityForResult(takePhotoIntent, PHOTO_REQUEST_CODE);
    }

    private Uri getPhotoFileUri(File dir) throws IOException
    {
        //coming up with a sufficient filename
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());   //i don't like this warning, but most of the timestamp solutions i see on the net do it this way
        String filename = "BtCoMKL_" + timeStamp;

        //Creating an empty file with our magnificent file name, in our dear directory, to put the image data into later. (I don't really think this sentence makes sense grammatically...) If i understand the tutorial right, that is... And it's sooo much better than doing it with createNewFile(), because i have no idea why, but i don't really have a choice, but to take the Android documentations word for it.
        File photoFile = File.createTempFile(filename, ".jpg", dir);

        //getting some cooler kind of reference to the file, because the reference we already have to this file isn't cool enough, Ok i mean this 'Uri' is short for Uniform Resource Identifier, so i suppose we made an instance of File, which is i guess a Java class made to handle files, but not a general path, understandable for anyone
        return FileProvider.getUriForFile(CreateNewCastle.this, "com.e.betweentwocastlesofmadkingludwigscoring.fileprovider", photoFile);
    }

    private void openAttendants()
    {
        backgroundShield.setVisibility(View.VISIBLE);
        cloAttendants.setVisibility(View.VISIBLE);
    }

    private void closeAttendants()
    {
        backgroundShield.setVisibility(View.GONE);
        cloAttendants.setVisibility(View.GONE);
    }

    private void handleAttendantClick(int attendantCode)
    {
        Button btnToSet;
        if(isFirstAttendantClicked)
        {
            btnToSet = btnAddFirstAttendant;
            btnAddSecondAttendant.setVisibility(View.VISIBLE);
            castle.setFirstAttendant(attendantCode);
        }
        else
        {
            btnToSet = btnAddSecondAttendant;
            castle.setSecondAttendant(attendantCode);
        }

        btnToSet.setText("");
        setAttendantButtonPicture(btnToSet, attendantCode);
        closeAttendants();
    }

    private void setAttendantButtonPicture(Button btnToSet, int attendantCode)
    {
        switch (attendantCode)
        {
            case 0:
                btnToSet.setBackgroundResource(R.drawable.attendant0);
                break;
            case 1:
                btnToSet.setBackgroundResource(R.drawable.attendant1);
                break;
            case 2:
                btnToSet.setBackgroundResource(R.drawable.attendant2);
                break;
            case 3:
                btnToSet.setBackgroundResource(R.drawable.attendant3);
                break;
        }
    }

    private void resetAttendantButton(Button btnToSet)
    {
        btnToSet.setBackgroundColor(getResources().getColor(R.color.skyBehindTowers));
        btnToSet.setText("+");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode == RESULT_OK && requestCode == PHOTO_REQUEST_CODE)
        {
            //TODO actually doing something with the photo i guess

            Toast.makeText(this, "Wow, nice castle!", Toast.LENGTH_LONG).show();
            txtInstructionPhoto = findViewById(R.id.txt_instruction_photo);
            txtInstructionPhoto.setVisibility(View.GONE);
            btnTakePhoto.setVisibility(View.GONE);
            scrViewCastleDetails.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NotNull String[] permissions, @NotNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == CAMERA_PERMISSION_CODE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED)
            {
                cameraPermission = true;
            }
            else
            {
                Toast.makeText(this, "Unfortunately we really need that camera, sorry... I mean the whole point is to do the scoring according to a photo.", Toast.LENGTH_LONG).show();
                cameraPermission = false;
            }
        }
    }

    @Override
    public void handleListItemClick(int index, int callingListCode)
    {
        if(callingListCode == ArrayAdapterGeneral.LIST_CODE_THRONE_ROOMS)
        {
            castle.setThroneRoom(index);
            listThroneRoom.setVisibility(View.GONE);
            backgroundShield.setVisibility(View.GONE);
            TypedArray throneRooms = getResources().obtainTypedArray(R.array.throne_rooms);
            btnSelectThroneRoom.setBackgroundResource(throneRooms.getResourceId(index,0));
            btnSelectThroneRoom.setText("");
            throneRooms.recycle();
        }
        else if(callingListCode == ArrayAdapterGeneral.LIST_CODE_AVAILABLE_BONUS_CARDS)
        {
            castle.addBonusCard(adapterAvailableBonusCards.getCurrentDataFromIndex(index));
            adapterSelectedBonusCards.addCard(adapterAvailableBonusCards.getCurrentDataFromIndex(index));
            adapterAvailableBonusCards.removeSelectedCard(index);
            listAvailableBonusCards.setVisibility(View.GONE);
            backgroundShield.setVisibility(View.GONE);
        }
        else if(callingListCode == ArrayAdapterGeneral.LIST_CODE_SELECTED_BONUS_CARDS_ADD)
        {
            backgroundShield.setVisibility(View.VISIBLE);
            listAvailableBonusCards.setVisibility(View.VISIBLE);
        }
        else if(callingListCode == ArrayAdapterGeneral.LIST_CODE_SELECTED_BONUS_CARD_REMOVED)
        {
            castle.removeBonusCard(adapterSelectedBonusCards.getCurrentDataFromIndex(index));
            adapterAvailableBonusCards.addUnselectedCard(adapterSelectedBonusCards.getCurrentDataFromIndex(index));
            adapterSelectedBonusCards.removeCard(index);
        }
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        adapterSelectedBonusCards.recyclePictureArray();
        adapterAvailableBonusCards.recyclePictureArray();
        adapterThroneRoom.recyclePictureArray();
    }

}