package com.e.betweentwocastlesofmadkingludwigscoring;

import android.net.Uri;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class Castle
{
    private int score;
    private String architectLeft;
    private String architectRight;
    private Date timeOfConstruction;
    private int throneRoom;
    private int firstAttendant = -1;
    private int secondAttendant = -1;
    private ArrayList<Integer> bonusCards = new ArrayList<>();
    private Uri pictureUri;

    /*
    //creating new castle with one attendant
    public Castle(String architectLeft, String architectRight, int throneRoom, int attendant, int[] bonusCards, Uri pictureUri)
    {
        setTimeOfConstruction();
        this.architectLeft = architectLeft;
        this.architectRight = architectRight;
        this.throneRoom = throneRoom;
        this.attendants = new int[]{attendant};
        this.bonusCards = bonusCards;
        this.pictureUri = pictureUri;
    }

    //creating new castle with two attendants
    public Castle(String architectLeft, String architectRight, int throneRoom, int attendant1, int attendant2, int[] bonusCards, Uri pictureUri)
    {
        setTimeOfConstruction();
        this.architectLeft = architectLeft;
        this.architectRight = architectRight;
        this.throneRoom = throneRoom;
        this.attendants = new int[]{attendant1, attendant2};
        this.bonusCards = bonusCards;
        this.pictureUri = pictureUri;
    }*/

    //getting an saved castle
//    public Castle(int score, String architectLeft, String architectRight, Date timeOfConstruction, int throneRoom, int firstAttendant, int secondAttendant, int[] bonusCards, Uri pictureUri)
//    {
//        this.score = score;
//        this.architectLeft = architectLeft;
//        this.architectRight = architectRight;
//        this.timeOfConstruction = timeOfConstruction;
//        this.throneRoom = throneRoom;
//        this.firstAttendant = firstAttendant;
//        this.secondAttendant = secondAttendant;
//        this.bonusCards = bonusCards;
//        this.pictureUri = pictureUri;
//    }

    //creating new castle
    public Castle()
    {
        setTimeOfConstruction();
    }

    public void setArchitectLeft(String architectLeft)
    {
        this.architectLeft = architectLeft;
    }

    public void setArchitectRight(String architectRight)
    {
        this.architectRight = architectRight;
    }

    public void setThroneRoom(int throneRoom)
    {
        this.throneRoom = throneRoom;
    }

    public void setFirstAttendant(int firstAttendant)
    {
        this.firstAttendant = firstAttendant;
    }

    public void setSecondAttendant(int secondAttendant)
    {
        this.secondAttendant = secondAttendant;
    }

    public void setBonusCards(ArrayList<Integer> bonusCards)
    {
        this.bonusCards = bonusCards;
    }

    public void addBonusCard(int id)
    {
        bonusCards.add(id);
    }

    public void removeBonusCard(int id) // i don't suppose this was absolutely necessary, but i simply couldn't wrap my head around how the compiler will decide if i meant "remove(Object o)" or "remove(int index)" if the objects in the list are integers, so i felt safer this way
    {
        int i = 0;
        boolean found = false;
        while (i < bonusCards.size() && !found)
        {
            if(bonusCards.get(i) == id)
            {
                bonusCards.remove(i);
                found = true;
            }
            i++;
        }
    }

    public void setPictureUri(Uri pictureUri)
    {
        this.pictureUri = pictureUri;
    }

    public void setScore(int score)
    {
        this.score = score;
    }

    private void setTimeOfConstruction()
    {
        this.timeOfConstruction = Calendar.getInstance().getTime();
    }


    public int getScore()
    {
        return score;
    }

    public String getArchitectLeft()
    {
        return architectLeft;
    }

    public String getArchitectRight()
    {
        return architectRight;
    }

    public Date getTimeOfConstruction()
    {
        return timeOfConstruction;
    }

    public int getThroneRoom()
    {
        return throneRoom;
    }

    public int getFirstAttendant()
    {
        return firstAttendant;
    }

    public int getSecondAttendant()
    {
        return secondAttendant;
    }

    public ArrayList<Integer> getBonusCards()
    {
        return bonusCards;
    }

    public Uri getPictureUri()
    {
        return pictureUri;
    }

    @Override
    public String toString()
    {
        return "Castle{" +
                "architectLeft='" + architectLeft + '\'' +
                ", architectRight='" + architectRight + '\'' +
                ", timeOfConstruction=" + timeOfConstruction +
                ", throneRoom=" + throneRoom +
                ", firstAttendant=" + firstAttendant +
                ", secondAttendant=" + secondAttendant +
                ", bonusCards=" + bonusCards +
                '}';
    }
}
