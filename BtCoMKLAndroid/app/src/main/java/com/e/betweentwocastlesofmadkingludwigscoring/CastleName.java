package com.e.betweentwocastlesofmadkingludwigscoring;

public class CastleName
{
    private enum State
    {
        ONE_VOWEL,
        TWO_VOWELS,
        ONE_CONSONANT,
        TWO_CONSONANTS
    }

    private enum Vibe
    {
        GERMAN,
        NORDIC,
        ENGLISH
    }

    private static String[] germanPostfixes = {"burg","stein","nitz","hof","dorf","bach","berg"};
    private static String[] nordicPostfixes = {"borg", "holm", "lund", "helm", "skov", "feld", "strup","linna", "slot"};
    private static String[] englishPostfixes = {"ton", "borough", "shire", "wood", "hold", "bridge", "stone", "hill", "ham", "town", "field"};

    private static String[] buildingTypes = {"Castle", "Palace", "Manor", "Tower", "Fort", "Fortress", "Hold", "Stronghold", "Citadel", "Keep", "Schloss", "Kastello"};

    //english
    private String[] EnglishUpperCaseVowels = {"E","U","I","O","A"};
    private String[] EnglishLowerCaseVowels = {"e","u","i","o","a"};
    private String[] EnglishUpperCaseConsonants = {"Qu","W","R","T","Z","P","S","D","F","G","H","J","K","L","C","V","B","N","M"};
    private String[] EnglishLowerCaseConsonants = {"qu","w","r","rr","t","tt","z","zz","p","pp","s","ss","d","dd","dz","f","ff","g","gg","h","k","l","ll","c","cc","v","b","n","nn","m"};
    private String[] EnglishSingleConsonants = {"w","r","t","z","p","s","d","f","g","h","k","l","c","v","b","n","m"};

    //german
    private String[] GermanUpperCaseVowels = {"E","U","I","O","A","Ü","Ö","Ä"};
    private String[] GermanLowerCaseVowels = {"e","u","i","o","a","ü","ö","ä"};
    private String[] GermanUpperCaseConsonants = {"Sch","Qu","W","R","T","Z","P","S","D","F","G","H","J","K","L","C","V","B","N","M"};
    private String[] GermanLowerCaseConsonants = {"sch", "ß","qu","w","r","rr","t","tt","z","zz","p","pp","s","d","dd","dz","f","ff","g","gg","h","k","l","ll","c","cc","v","b","n","nn","m"};
    private String[] GermanSingleConsonants = {"ß","w","r","t","z","p","s","d","f","g","h","k","l","c","v","b","n","m"};

    //nordic
    private String[] NordicUpperCaseVowels = {"Y","Ø","Å","E","U","I","O","A"};
    private String[] NordicLowerCaseVowels = {"y","ø","å","e","u","i","o","a"};
    private String[] NordicUpperCaseConsonants = {"Qu","W","R","T","Z","P","S","D","F","G","H","J","K","L","C","V","B","N","M"};
    private String[] NordicLowerCaseConsonants = {"qu","w","r","rr","t","tt","z","zz","p","pp","s","ss","d","dd","dz","f","ff","g","gg","h","k","l","ll","c","cc","v","b","n","nn","m"};
    private String[] NordicSingleConsonants = {"w","r","t","z","p","s","d","f","g","h","k","l","c","v","b","n","m"};

    //current
    private String[] UpperCaseVowels;
    private String[] LowerCaseVowels;
    private String[] UpperCaseConsonants;
    private String[] LowerCaseConsonants;
    private String[] SingleConsonants;



    private Vibe vibe;
    private State state;
    private String castleName = "";
    private String lastLetter;

    public String GenerateCastleName()
    {
        vibe = Vibe.values()[getRandom(3)];
        switch (vibe)
        {
            case GERMAN:
                UpperCaseVowels = GermanUpperCaseVowels;
                LowerCaseVowels = GermanLowerCaseVowels;
                UpperCaseConsonants = GermanUpperCaseConsonants;
                LowerCaseConsonants = GermanLowerCaseConsonants;
                SingleConsonants = GermanSingleConsonants;
                break;
            case NORDIC:
                UpperCaseVowels = NordicUpperCaseVowels;
                LowerCaseVowels = NordicLowerCaseVowels;
                UpperCaseConsonants = NordicUpperCaseConsonants;
                LowerCaseConsonants = NordicLowerCaseConsonants;
                SingleConsonants = NordicSingleConsonants;
                break;
            case ENGLISH:
                UpperCaseVowels = EnglishUpperCaseVowels;
                LowerCaseVowels = EnglishLowerCaseVowels;
                UpperCaseConsonants = EnglishUpperCaseConsonants;
                LowerCaseConsonants = EnglishLowerCaseConsonants;
                SingleConsonants = EnglishSingleConsonants;
                break;
        }

        String firstLetters = getFirstLetters();
        castleName += firstLetters;

        int length = getRandom(4)+1;
        for (int i= 0; i<length; i++)
        {
            String nextLetter = getNextLetter();
            castleName = castleName + nextLetter;
            lastLetter = nextLetter;
        }

        getEnding();

        return castleName;
    }

    private String getFirstLetters()
    {
        String firstLetter;
        String secondLetter;
        if(isNextVowel())
        {
            firstLetter = getRandomLetter(UpperCaseVowels);
            secondLetter = getRandomLetter(LowerCaseConsonants);
            if(secondLetter.length() == 1)
            {
                state = State.ONE_CONSONANT;
            }
            else
            {
                state = State.TWO_CONSONANTS;
            }
        }
        else
        {
            firstLetter = getRandomLetter(UpperCaseConsonants);
            secondLetter = getRandomLetter(LowerCaseVowels);
            state = State.ONE_VOWEL;
        }
        lastLetter = secondLetter;
        return firstLetter + secondLetter;
    }

    private String getNextLetter()
    {
        String nextLetter ="";
        switch (state)
        {
            case ONE_VOWEL:
                if(isNextVowel())
                {
                    nextLetter = getPreferablyDifferentVowel();
                    state = State.TWO_VOWELS;
                }
                else
                {
                    nextLetter = getConsonant();
                }
                break;
            case TWO_VOWELS:
                nextLetter = getConsonant();
                break;
            case ONE_CONSONANT:
                if(isNextVowel())
                {
                    nextLetter = getVowel();
                    state = State.ONE_VOWEL;
                }
                else
                {
                    nextLetter = getDifferentConsonant();
                }
                break;
            case TWO_CONSONANTS:
                nextLetter = getRandomLetter(LowerCaseVowels);
                state = State.ONE_VOWEL;
                break;
        }
        return nextLetter;
    }

    private int getRandom(int max)
    {
        return (int)Math.floor(Math.random()*max);
    }

    private String getRandomLetter(String[] letterList)
    {
        int index = getRandom(letterList.length);
        return letterList[index];
    }

    private boolean isNextVowel()
    {
        return getRandom(LowerCaseVowels.length + LowerCaseConsonants.length) < LowerCaseVowels.length;
    }

    private String getVowel()
    {
        return getRandomLetter(LowerCaseVowels);
    }

    private String getPreferablyDifferentVowel()
    {

        String nextLetter = getRandomLetter(LowerCaseVowels);
        if(nextLetter.equals(lastLetter))
        {
            nextLetter = getRandomLetter(LowerCaseVowels);
        }
        return nextLetter;
    }

    private String getConsonant()
    {
        String nextLetter = getRandomLetter(LowerCaseConsonants);
        if(nextLetter.length()==1)
        {
            state = State.ONE_CONSONANT;
        }
        else
        {
            state = State.TWO_CONSONANTS;
        }
        return nextLetter;
    }

    private String getDifferentConsonant()
    {
        String nextLetter;
        do
        {
            nextLetter = getSingleConsonant();
        }while (nextLetter.contains(lastLetter));
        state = State.TWO_CONSONANTS;
        return nextLetter;
    }

    private String getSingleConsonant()
    {
        return getRandomLetter(SingleConsonants);
    }

    private void getEnding()
    {
        switch (state)
        {
            case ONE_VOWEL:
            case TWO_VOWELS:
                castleName += getSingleConsonant();
                break;
            case ONE_CONSONANT:
                if(getRandom(LowerCaseVowels.length + LowerCaseConsonants.length + 1) == 0)
                {
                    castleName += "ing";
                }
                break;
            case TWO_CONSONANTS:
                if(getRandom(LowerCaseVowels.length + LowerCaseConsonants.length + 1) == 0)
                {
                    castleName += "ing";
                }
                else
                {
                    castleName += getVowel() + getSingleConsonant();
                }
                break;
        }

        switch (vibe)
        {
            case NORDIC:
                castleName += nordicPostfixes[getRandom(nordicPostfixes.length)];
                break;
            case GERMAN:
                castleName += germanPostfixes[getRandom(germanPostfixes.length)];
                break;
            case ENGLISH:
                castleName += englishPostfixes[getRandom(englishPostfixes.length)];
                break;
        }

        castleName += " ";
        castleName += buildingTypes[getRandom((buildingTypes.length))];
    }
}
