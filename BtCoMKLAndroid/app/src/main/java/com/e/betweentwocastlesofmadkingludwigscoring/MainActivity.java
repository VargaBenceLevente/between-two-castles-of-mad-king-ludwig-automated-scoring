package com.e.betweentwocastlesofmadkingludwigscoring;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity
{

    private Button btnNewCastle;
    private Button btnPreviousCastles;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnNewCastle = findViewById(R.id.btn_new_castle);
        btnPreviousCastles = findViewById(R.id.btn_previous_castles);

        btnNewCastle.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(MainActivity.this, CreateNewCastle.class);
                startActivity(intent);
            }
        });

        btnPreviousCastles.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(MainActivity.this, PreviousCastles.class);
                startActivity(intent);
            }
        });
    }
}