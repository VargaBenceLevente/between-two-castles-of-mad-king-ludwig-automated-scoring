package com.e.betweentwocastlesofmadkingludwigscoring.lists;

import android.content.Context;

import com.e.betweentwocastlesofmadkingludwigscoring.listItemClickHandler;
import com.e.betweentwocastlesofmadkingludwigscoring.R;

public class ArrayAdapterThroneRoom extends ArrayAdapterSinglePictureList
{
    public ArrayAdapterThroneRoom(Context context, listItemClickHandler activity)
    {
        super(context, activity, ArrayAdapterGeneral.LIST_CODE_THRONE_ROOMS, R.array.throne_rooms);
        fillInDataWithInts();
    }
}
