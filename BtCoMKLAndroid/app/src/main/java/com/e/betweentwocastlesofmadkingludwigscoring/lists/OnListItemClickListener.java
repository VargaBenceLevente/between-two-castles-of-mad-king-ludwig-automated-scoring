package com.e.betweentwocastlesofmadkingludwigscoring.lists;

import android.view.View;

import com.e.betweentwocastlesofmadkingludwigscoring.listItemClickHandler;

public class OnListItemClickListener implements View.OnClickListener
{
    private int index;
    private listItemClickHandler clickHandler;
    private int listCode;

    public OnListItemClickListener(int index, listItemClickHandler clickHandler, int listCode)
    {
        this.index = index;
        this.clickHandler = clickHandler;
        this.listCode = listCode;
    }

    @Override
    public void onClick(View view)
    {
        clickHandler.handleListItemClick(index,listCode);
    }
}
