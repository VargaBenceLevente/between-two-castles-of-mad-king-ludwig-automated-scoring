package com.e.betweentwocastlesofmadkingludwigscoring.lists;

import android.content.Context;
import android.content.res.TypedArray;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.e.betweentwocastlesofmadkingludwigscoring.listItemClickHandler;
import com.e.betweentwocastlesofmadkingludwigscoring.R;

import java.util.ArrayList;

public class ArrayAdapterSelectedBonusCards extends ArrayAdapterGeneral<Integer>
{
    TypedArray picturesArray;

    public ArrayAdapterSelectedBonusCards(Context context, listItemClickHandler activity)
    {
        super(context, R.layout.list_item_selected_bonuscard, activity);
        data = new ArrayList<>();
    }

    @Override
    public void setLlyo(LinearLayout llyo)
    {
        super.setLlyo(llyo);
        llyo.setOrientation(LinearLayout.HORIZONTAL);
    }

    @Override
    protected void populateList()
    {
        super.populateList();
        Button btnPlus = new Button(context);
        btnPlus.setText("+");
        btnPlus.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                activity.handleListItemClick(-1, LIST_CODE_SELECTED_BONUS_CARDS_ADD);
            }
        });
        llyo.addView(btnPlus);
    }

    @Override
    protected View arrangeDataInView(View currentListItem, int index)
    {
        int currentData = data.get(index);
        this.picturesArray = context.getResources().obtainTypedArray(R.array.bonus_cards);

        ImageView btnSingleImage = currentListItem.findViewById(R.id.img_selected_bonus_card);
        btnSingleImage.setBackgroundResource(picturesArray.getResourceId(currentData,0));

        Button btnMinus = currentListItem.findViewById(R.id.btn_remove_bonuscard);
        btnMinus.setOnClickListener(new OnListItemClickListener(index, activity, LIST_CODE_SELECTED_BONUS_CARD_REMOVED));
        return currentListItem;
    }

    public int getCurrentDataFromIndex(int index)
    {
        return data.get(index);
    }

    public void addCard(int cardId)
    {
        data.add(cardId);
        updateList();
    }

    public void removeCard(int index)
    {
        data.remove(index);
        updateList();
    }

    public void recyclePictureArray()
    {
        picturesArray.recycle();
    }
}
