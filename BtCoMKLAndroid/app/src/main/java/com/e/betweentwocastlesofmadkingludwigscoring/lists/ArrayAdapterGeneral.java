package com.e.betweentwocastlesofmadkingludwigscoring.lists;

//Dear me! I don't only do this to match the official Android ways, but because these lists have
//lots and lots of attributes, that should go to a constructor, but since they are child classes of class
//View, we cant really mess with their constructor.
//But we can create a whole new type of classes, ArrayAdapter, we can ask for whatever we want in their
//constructors, then create a mere function in the list classes, setArrayAdapter, to connect the needed
//attributes with the lists that need them.
//Dear me, these are the reasons I came to this conclusion for the third time now. Use this description,
//and if you shall find that I'm wrong, create something even better! Best of luck!

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import com.e.betweentwocastlesofmadkingludwigscoring.listItemClickHandler;

import java.util.ArrayList;

public abstract class ArrayAdapterGeneral<T>
{
    public static final int LIST_CODE_THRONE_ROOMS = 102;
    public static final int LIST_CODE_AVAILABLE_BONUS_CARDS = 103;
    public static final int LIST_CODE_SELECTED_BONUS_CARDS_ADD = 104;
    public static final int LIST_CODE_SELECTED_BONUS_CARD_REMOVED = 105;

    protected Context context;
    protected int listCode;
    protected int resource;
    protected ArrayList<T> data;
    protected listItemClickHandler activity;
    protected LinearLayout llyo;

    public ArrayAdapterGeneral(Context context, int listCode, int resource, listItemClickHandler activity)
    {
        this.context = context;
        this.listCode = listCode;
        this.resource = resource;
        this.activity = activity;
    }

    public ArrayAdapterGeneral(Context context, int resource, listItemClickHandler activity)
    {
        this.context = context;
        this.resource = resource;
        this.activity = activity;
    }

    public ArrayAdapterGeneral(Context context, ArrayList<T> data, listItemClickHandler activity)
    {
        this.context = context;
        this.data = data;
        this.activity = activity;
    }

    public void setLlyo(LinearLayout llyo)
    {
        this.llyo = llyo;
    }

    protected void populateList ()
    {
        llyo.removeAllViews();
        for(int i = 0; i < data.size();i++)
        {
            llyo.addView(getView(i));
        }
    }

    private View getView(int index)
    {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View currentListItem = layoutInflater.inflate(resource, null);
        return arrangeDataInView(currentListItem, index);
    }

    protected abstract View arrangeDataInView(View currentListItem, int index);

    public void updateList()
    {
        populateList();
    }
}
