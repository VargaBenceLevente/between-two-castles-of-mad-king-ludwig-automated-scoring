package com.e.betweentwocastlesofmadkingludwigscoring.lists;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ArrayAdapter;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;

//This is an ugly solution. This code is the very same, as the code in "ListGeneral". But i couldn't think of anything else, to make it scroll horizontally yet. I'm sorry.

public class ListSelectedBonusCards extends HorizontalScrollView implements ViewWithAdapter<Integer>
{
    protected ArrayAdapterGeneral<Integer> adapter;
    protected Context context;
    protected LinearLayout llyo;

    public ListSelectedBonusCards(Context context)
    {
        super(context);
        init(context);
    }

    public ListSelectedBonusCards(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        init(context);
    }

    public ListSelectedBonusCards(Context context, AttributeSet attrs, int defStyleAttr)
    {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    public ListSelectedBonusCards(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes)
    {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context);
    }

    protected void init(Context context)
    {
        this.context = context;
        llyo = new LinearLayout(context);
        llyo.setOrientation(LinearLayout.VERTICAL);
        this.addView(llyo);
    }


    @Override
    public void setAdapter(ArrayAdapterGeneral<Integer> adapter)
    {
        this.adapter = adapter;
        adapter.setLlyo(llyo);
        adapter.populateList();
    }
}
