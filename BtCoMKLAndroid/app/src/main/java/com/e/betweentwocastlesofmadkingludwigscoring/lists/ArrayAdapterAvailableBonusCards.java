package com.e.betweentwocastlesofmadkingludwigscoring.lists;

import android.content.Context;

import com.e.betweentwocastlesofmadkingludwigscoring.listItemClickHandler;
import com.e.betweentwocastlesofmadkingludwigscoring.R;

public class ArrayAdapterAvailableBonusCards extends ArrayAdapterSinglePictureList
{

    public ArrayAdapterAvailableBonusCards(Context context, listItemClickHandler activity)
    {
        super(context, activity, LIST_CODE_AVAILABLE_BONUS_CARDS, R.array.bonus_cards);
        fillInDataWithInts();
    }

    public void removeSelectedCard(int index)
    {
        data.remove(index);
        updateList();
    }

    public void addUnselectedCard(int id)
    {
        data.add(id);
        updateList();
    }

    public int getCurrentDataFromIndex(int index)
    {
        return data.get(index);
    }
}
